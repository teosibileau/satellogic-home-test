import os

import json
import operator

from logger import Logger
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
from metrics import Metrics


class Queue(object):
    def __init__(self, satellite_id):
        self.satellite = satellite_id
        self.resources_being_used = []
        self.tasks = []

    def subscribe(self, task):
        resources_collide = len([
            i for i in task['recursos']
            if i in self.resources_being_used
        ])
        if resources_collide:
            return False
        self.resources_being_used += task['recursos']
        self.tasks.append(task)
        return True

    def broadcast(self, client):
        topic = 'satellite/%s/tasks' % self.satellite
        Logger.logger().info(
            "Broadcasting tasks to %s" % topic
        )
        client.publish(
            'satellite/%s/tasks' % self.satellite,
            json.dumps(self.tasks)
        )


class SpaceControl(mqtt.Client):
    satellites = []
    _metrics = None

    def __init__(self, *args, **kwargs):
        super(SpaceControl, self).__init__()
        self.on_connect = SpaceControl.on_connect_callback
        self.message_callback_add(
            'spacecontrol/tasks',
            SpaceControl.prioritize_tasks
        )
        self.message_callback_add(
            'satellite/connected',
            SpaceControl.register_satellite
        )
        Logger.logger().info('Initializing connection')
        self.connect(
            os.environ.get("MQTT_HOST", "0.0.0.0"),
            int(os.environ.get("MQTT_PORT", 1883)),
            int(os.environ.get("MQTT_KEEPALIVE", 60))
        )

    @classmethod
    def metrics(cls):
        if not SpaceControl._metrics:
            SpaceControl._metrics = Metrics('tasks')
        return SpaceControl._metrics

    @classmethod
    def on_connect_callback(cls, client, userdata, flags, rc):
        Logger.logger().info("Connected with result code " + str(rc))
        client.subscribe("satellite/connected")
        client.subscribe("spacecontrol/tasks")
        publish.single(
            'spacecontrol/connected',
            'ok',
            hostname=os.environ.get("MQTT_HOST", "0.0.0.0"),
            port=int(os.environ.get("MQTT_PORT", 1883)),
            keepalive=int(os.environ.get("MQTT_KEEPALIVE", 60))
        )

    @classmethod
    def register_satellite(cls, client, userdata, message):
        clientid = message.payload.decode('utf-8')
        if clientid not in SpaceControl.satellites:
            SpaceControl.satellites.append(clientid)
            client.message_callback_add(
                'satellite/%s/responses' % clientid,
                SpaceControl.process_responses
            )
            client.subscribe("satellite/%s/responses" % clientid)
            Logger.logger().info(
                "Satellite %s registered on SpaceControl" % clientid
            )

    @classmethod
    def prioritize_tasks(cls, client, userdata, message):
        tasks = json.loads(message.payload)
        if len(SpaceControl.satellites):
            tasks = sorted(tasks, key=operator.itemgetter('payoff'))
            queues = [
                Queue(satellite)
                for satellite in SpaceControl.satellites
            ]
            while len(tasks):
                task = tasks.pop()
                task.update({'subscribed': False, 'succesful': False})
                for queue in queues:
                    if queue.subscribe(task):
                        task['subscribed'] = True
                        Logger.logger().info(
                            "Task %s subscribed to %s" % (
                                task['uuid'],
                                queue.satellite
                            )
                        )
                        break
                if not task['subscribed']:
                    Logger.logger().info(
                        "Task %s resources collide" % (
                            task['uuid']
                        )
                    )
                    SpaceControl.persist_task(task)

            for queue in queues:
                queue.broadcast(client)
        else:
            Logger.logger().info('No satellites available to broadcast tasks')

    @classmethod
    def process_responses(cls, client, userdata, message):
        satellite = message.topic.split('/')[1]
        Logger.logger().info(
            'Processing response from satellite %s' % satellite
        )
        task = json.loads(message.payload)
        task['satellite'] = satellite
        SpaceControl.persist_task(task)

    @classmethod
    def persist_task(cls, task):
        metrics = SpaceControl.metrics()
        p = {
            'fields': {
                'payoff': float(task['payoff']),
            },
            'tags': {
                'nombre': task['nombre'],
                'satellite': task.get('satellite', 'None'),
                'subscribed': task['subscribed'],
                'succesful': task['succesful']
            }
        }
        for i in range(len(task['recursos'])):
            p['fields'].update({'resource_%s' % i: task['recursos'][i]})
        metrics.report_points([p])
