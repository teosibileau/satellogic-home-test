import os
import pytz
from datetime import datetime
from influxdb import InfluxDBClient


class Metrics(InfluxDBClient):
    def __init__(self, metric='generic'):
        super(Metrics, self).__init__(
            host=os.environ.get('INFLUXDB_HOST', '0.0.0.0'),
            port=os.environ.get('INFLUXDB_PORT', 8086),
            username=os.environ.get('INFLUXDB_USER', 'admin'),
            password=os.environ.get('INFLUXDB_USER_PASSWORD', 'admin123'),
            database=os.environ.get('INFLUXDB_DB', 'metrics'),
            ssl=False
        )
        self.metric = metric

    def report(self, fields, tags, timestamp=False):
        d = {
            'fields': fields,
            'tags': tags,
            'measurement': self.metric,
        }
        if timestamp:
            d['time'] = timestamp
        self.report_points([d])

    def report_points(self, points=[]):
        for i in range(len(points)):
            p = points[i]
            if 'time' not in p:
                u = datetime.utcnow()
                u = u.replace(tzinfo=pytz.utc)
                p['time'] = u
            p['time'] = str(p['time'])
            if 'measurement' not in p:
                p['measurement'] = self.metric
            points[i] = p
        if len(points):
            r = self.write_points(points)
            return r
        return False
