import logging
import daiquiri


class Logger(object):
    _logger = None

    @classmethod
    def logger(cls):
        if not Logger._logger:
            formatter = daiquiri.formatter.ColorFormatter(
                fmt="%(asctime)s [PID %(process)d] [%(levelname)s] "
                "%(name)s -> %(message)s")
            daiquiri.setup(
                level=logging.INFO,
                outputs=(daiquiri.output.Stream(formatter=formatter),)
            )
            Logger._logger = daiquiri.getLogger('spacecontrol')
        return Logger._logger
