import os
import json
from settings import *


def backup_dashboards():
    dashboards = GRAFANA_CLIENT.search()
    dashboards = [
        dashboard for dashboard in dashboards
        if dashboard['type'] == 'dash-db'
    ]
    for dashboard in dashboards:
        export = GRAFANA_CLIENT.make_raw_request(
            'GET', 'dashboards/%s' % dashboard['uri'], {})
        f = os.path.join(BASE_DIR, 'dashboards',
                         '%s.json' % dashboard['uri'].replace('db/', ''))
        with open(f, 'w+') as outfile:
            json.dump(export, outfile, indent=4)


if __name__ == "__main__":
    backup_dashboards()
