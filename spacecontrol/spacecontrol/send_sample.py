import os
import uuid
import json
import random
import paho.mqtt.publish as publish
import tabulate


task_types = {
    'fotos': {
        'payoff': (10, 20),
        'recursos': [1, 5]
    },
    'mantenimiento': {
        'payoff': (1, 2),
        'recursos': [1, 2],
    },
    'pruebas': {
        'payoff': (0.1, 1),
        'recursos': [5, 6]
    },
    'fsck': {
        'payoff': (0.1, 0.4),
        'recursos': [1, 6]
    }
}

RANDOM_TASKS = random.randint(3, 10)

tasks = []

for i in range(RANDOM_TASKS):
    keys = list(task_types.keys())
    k = random.choice(keys)
    task = {
        'nombre': k,
        'payoff': random.uniform(
            task_types[k]['payoff'][0],
            task_types[k]['payoff'][1]
        ),
        'recursos': task_types[k]['recursos'],
        'uuid': uuid.uuid4().hex
    }
    tasks.append(task)


publish.single(
    'spacecontrol/tasks',
    json.dumps(tasks),
    hostname=os.environ.get("MQTT_HOST", "0.0.0.0"),
    port=int(os.environ.get("MQTT_PORT", 1883)),
    keepalive=int(os.environ.get("MQTT_KEEPALIVE", 60))
)

output = []

for t in tasks:
    output.append([
        t['nombre'],
        t['payoff'],
        t['recursos'],
        t['uuid'],
    ])

output = tabulate.tabulate(
    output,
    headers=['Nombre', 'Payoff', 'Recursos', 'Task UUID']
)

print(output)
